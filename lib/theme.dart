import 'package:flutter/material.dart';
import 'package:coffee_with_me/utils/utils.dart';

ThemeData buildThemeData(){
  final baseTheme = ThemeData(
    fontFamily: AvailableFonts.primaryFont
  );
  return baseTheme.copyWith();
}