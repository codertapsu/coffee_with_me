import 'package:flutter/material.dart';

class StyleConfig {
  static const UnderlineInputBorder underLineInputStyle = UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  );
}
