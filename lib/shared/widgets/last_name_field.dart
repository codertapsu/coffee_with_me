import 'package:coffee_with_me/bloc/auth_bloc.dart';
import 'package:coffee_with_me/shared/styles/style_config.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

Widget lastNameField(BuildContext context) {
  final authBloc = Provider.of<AuthBloc>(context);
  return StreamBuilder<Object>(
    stream: authBloc.lastName,
    builder: (BuildContext context, snapshot) {
      return TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          focusedBorder: StyleConfig.underLineInputStyle,
          labelText: 'last name',
          labelStyle: TextStyle(color: Colors.white),
          errorText: snapshot.error,
          prefixIcon: Icon(
            LineIcons.pencil,
            color: Colors.white,
          ),
        ),
        style: TextStyle(color: Colors.white),
        onChanged: authBloc.lastNameChanges,
      );
    },
  );
}
