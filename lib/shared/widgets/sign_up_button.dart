import 'package:coffee_with_me/bloc/auth_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Widget signUpButton(
    BuildContext context, Function(Map<String, String>) callbackSubmit) {
  final AuthBloc authBloc = Provider.of(context);
  return StreamBuilder<Object>(
    stream: authBloc.submitSignUpButtonValid,
    builder: (BuildContext context, AsyncSnapshot snapshot) {
      return Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(7),
          color: Colors.white,
        ),
        child: RaisedButton(
          onPressed: () => callbackSubmit(authBloc.submitSignUp()),
          disabledColor: Colors.grey,
          disabledTextColor: Colors.white,
          color: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
          child: Text(
            'Sign up',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 20,
            ),
          ),
        ),
      );
    },
  );
}
