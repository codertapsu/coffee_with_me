import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  Loading({this.isLoading});
  final bool isLoading;
  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(
        child: new CircularProgressIndicator(),
      );
    }
    return new Container(
      height: 0,
      width: 0,
    );
  }
}
