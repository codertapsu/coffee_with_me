import 'package:coffee_with_me/shared/constant/constant.config.dart';
import 'package:flutter/material.dart';

import '../primary_button.dart';
import 'calendar.view.dart';

class DatePickerPopup extends StatefulWidget {
  final bool barrierDismissible;
  final Function() onApplyClick;
  final Function onCancelClick;
  DatePickerPopup({
    Key key,
    this.barrierDismissible,
    this.onApplyClick,
    this.onCancelClick,
  }) : super(key: key);

  _DatePickerPopupState createState() => _DatePickerPopupState();
}

class _DatePickerPopupState extends State<DatePickerPopup>
    with TickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    _animationController =
        AnimationController(duration: Duration(milliseconds: 400), vsync: this);
    _animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: AnimatedBuilder(
          animation: _animationController,
          builder: (BuildContext context, Widget child) {
            return AnimatedOpacity(
              duration: Duration(milliseconds: 100),
              opacity: _animationController.value,
              child: InkWell(
                splashColor: Colors.transparent,
                focusColor: Colors.transparent,
                highlightColor: Colors.transparent,
                hoverColor: Colors.transparent,
                onTap: () {
                  if (widget.barrierDismissible) {
                    Navigator.pop(context);
                  }
                },
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(
                            Radius.circular(24.0),
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              offset: Offset(4, 4),
                              blurRadius: 8.0,
                            )
                          ]),
                      child: Padding(
                        padding: EdgeInsets.only(top: 15, bottom: 15),
                        child: InkWell(
                          onTap: () {},
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              CalendarView(),
                              SizedBox(height: 10),
                              Container(
                                width: double.infinity,
                                child: Padding(
                                  padding: EdgeInsets.only(left: 15, right: 15),
                                  child: FlatButton(
                                    padding:
                                        EdgeInsets.only(top: 10, bottom: 10),
                                    onPressed: () {},
                                    color: Colors.blue[300],
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                    ),
                                    child: Text(
                                      Constants.apply_text,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
