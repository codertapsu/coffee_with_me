import 'package:coffee_with_me/shared/constant/constant.config.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CalendarView extends StatefulWidget {
  final bool isRangePicker;
  final DateTime minDate;
  final DateTime maxDate;
  final DateTime initMinDate;
  final DateTime initMaxDate;
  final Function(DateTime, DateTime) onDateChange;
  CalendarView({
    Key key,
    this.isRangePicker = true,
    this.minDate,
    this.maxDate,
    this.initMinDate,
    this.initMaxDate,
    this.onDateChange,
  }) : super(key: key);

  _CalendarViewState createState() => _CalendarViewState();
}

class _CalendarViewState extends State<CalendarView> {
  List<DateTime> _dateList = List<DateTime>();
  DateTime _currentMonthDate = DateTime.now();
  bool _isRangePicker;
  DateTime _startDate;
  DateTime _endDate;
  DateTime _selectedDate = DateTime.now();
  int _indexWeekInMonth;

  void _setCurrentMonth({bool isNext = true}) {
    final step = isNext ? 2 : 0;
    setState(() {
      _currentMonthDate =
          DateTime(_currentMonthDate.year, _currentMonthDate.month + step, 0);
    });
    _setNumberDayOfMonth(_currentMonthDate);
  }

  void _setCurrentYear({bool isNext = true}) {
    final step = isNext ? 1 : -1;
    setState(() {
      _currentMonthDate = DateTime(
          _currentMonthDate.year + step, _currentMonthDate.month + 1, 0);
      print('${_currentMonthDate.toString()}');
    });
    _setNumberDayOfMonth(_currentMonthDate);
  }

  void _setNumberDayOfMonth(DateTime currentMonth) {
    _dateList.clear();
    final newDate = DateTime(currentMonth.year, currentMonth.month, 0);
    int prevMonthDay = 0;
    if (newDate.weekday < 7) {
      prevMonthDay = newDate.weekday;
      for (var i = 1; i <= prevMonthDay; i++) {
        _dateList.add(newDate.subtract(Duration(days: prevMonthDay - i)));
      }
    }
    for (var i = 0; i < (42 - prevMonthDay); i++) {
      _dateList.add(newDate.add(Duration(days: i + 1)));
    }
    if (_indexWeekInMonth == null) {
      final initIndexCurrentDate =
          _dateList.indexWhere((item) => item.day == _selectedDate.day);
      if (initIndexCurrentDate > 0) {
        setState(() {
          _indexWeekInMonth = initIndexCurrentDate ~/ 7;
        });
      }
    }
  }

  void _onSelectDate(DateTime date) {
    if (date.month != _currentMonthDate.month) {
      date.year < _currentMonthDate.year
          ? _setCurrentMonth(isNext: false)
          : date.year > _currentMonthDate.year
              ? _setCurrentMonth()
              : date.month < _currentMonthDate.month
                  ? _setCurrentMonth(isNext: false)
                  : _setCurrentMonth();
    }
    _isRangePicker == true ? _onSetRangeDate(date) : _onSetSingleDate(date);
  }

  void _onSetSingleDate(DateTime date) {
    setState(() {
      _selectedDate = date;
      _indexWeekInMonth = _dateList.indexOf(date) ~/ 7;
    });
    widget.onDateChange(date, null);
  }

  void _onSetRangeDate(DateTime date) {
    if (_startDate != null && _endDate != null) {
      if (date.isBefore(_startDate)) {
        setState(() {
          _endDate = _startDate;
          _startDate = date;
        });
      } else {
        int diffToStartDate = date.difference(_startDate).inDays;
        int diffToEndDate = date.difference(_endDate).inDays;
        bool isCloserStartDate =
            (diffToStartDate < 0 ? -diffToStartDate : diffToStartDate) <
                (diffToEndDate < 0 ? -diffToEndDate : diffToEndDate);
        setState(() {
          isCloserStartDate ? _startDate = date : _endDate = date;
        });
      }
    } else {
      setState(() {
        _startDate != null ? _endDate = date : _startDate = date;
      });
    }
    widget.onDateChange(_startDate, _endDate);
  }

  bool _isCurrentDate(DateTime date) {
    return (_selectedDate != null &&
            _selectedDate.day == date.day &&
            _selectedDate.month == date.month &&
            _selectedDate.year == date.year)
        ? true
        : false;
  }

  bool _isStartOrEndDate(DateTime date) {
    return ((_startDate != null && date.isAtSameMomentAs(_startDate)) ||
            (_endDate != null && date.isAtSameMomentAs(_endDate)))
        ? true
        : false;
  }

  bool _isDayInCurrentMonth(DateTime date) {
    return (_currentMonthDate.month != null &&
            _currentMonthDate.month == date.month)
        ? true
        : false;
  }

  bool _isDayInRange(DateTime date) {
    return (_startDate != null &&
            _endDate != null &&
            date.isBefore(_endDate) &&
            date.isAfter(_startDate))
        ? true
        : false;
  }

  bool _isSameWeek(int indexWeekInMonth, int iteratorWeekInMonth) {
    return _dateList.indexWhere((item) => (item.year == _selectedDate.year &&
                item.month == _selectedDate.month &&
                item.day == _selectedDate.day)) >=
            0 &&
        _indexWeekInMonth != null &&
        _indexWeekInMonth == iteratorWeekInMonth;
  }

  Color _setColorPoint(DateTime date) {
    return _isRangePicker == true
        ? (_isStartOrEndDate(date) ? Colors.white : Colors.transparent)
        : (_isCurrentDate(date) ? Colors.white : Colors.transparent);
  }

  Color _setColorTextPick(DateTime date) {
    return _isRangePicker == true
        ? (_isStartOrEndDate(date)
            ? Colors.white
            : (_isDayInRange(date)
                ? Colors.blue[300]
                : (_isDayInCurrentMonth(date)
                    ? Colors.black54
                    : Colors.black38)))
        : (_isCurrentDate(date)
            ? Colors.white
            : _isDayInCurrentMonth(date) ? Colors.white : Colors.white24);
  }

  Color _setBgDaySelected(DateTime date) {
    return _isCurrentDate(date)
        ? Color.fromRGBO(232, 53, 126, 1)
        : Colors.transparent;
  }

  Color _setRadiusColorDaySelected(DateTime date) {
    return _isCurrentDate(date)
        ? Color.fromRGBO(232, 53, 126, 0.2)
        : Colors.transparent;
  }

  @override
  void initState() {
    _setNumberDayOfMonth(_currentMonthDate);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Color.fromRGBO(20, 32, 99, 1)),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 8, left: 8),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Row(
                    children: <Widget>[
                      _directionDate(
                        Icons.keyboard_arrow_left,
                        () => _setCurrentMonth(isNext: false),
                      ),
                      Text(
                        DateFormat(Constants.format_month_short)
                            .format(_currentMonthDate)
                            .toUpperCase(),
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                      _directionDate(
                        Icons.keyboard_arrow_right,
                        () => _setCurrentMonth(),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Center(
                    child: FlatButton(
                      onPressed: () {},
                      padding: EdgeInsets.all(2),
                      color: Colors.transparent,
                      shape:
                          CircleBorder(side: BorderSide(color: Colors.white)),
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Row(
                    children: <Widget>[
                      _directionDate(
                        Icons.keyboard_arrow_left,
                        () => _setCurrentYear(isNext: false),
                      ),
                      Text(
                        DateFormat(Constants.format_year_long)
                            .format(_currentMonthDate)
                            .toUpperCase(),
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                      _directionDate(
                        Icons.keyboard_arrow_right,
                        () => _setCurrentYear(),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 8, left: 8, bottom: 8),
            child: Row(
              children: _getDaysNameUI(),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 8, left: 8),
            child: Column(
              children: _getDaysNoUI(),
            ),
          ),
        ],
      ),
    );
  }

  Widget _directionDate(IconData iconDirection, Function pressCb) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 38,
        width: 38,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(24)),
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: BorderRadius.all(Radius.circular(24)),
            onTap: pressCb,
            child: Icon(
              iconDirection,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _getDaysNameUI() {
    List<Widget> listUI = List<Widget>();
    for (var i = 0; i < 7; i++) {
      listUI.add(Expanded(
        child: Center(
          child: Text(
            DateFormat(Constants.format_day_name_short).format(_dateList[i]),
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: _selectedDate.weekday - 1 == i ? Colors.green : Colors.green[300],
            ),
          ),
        ),
      ));
    }
    return listUI;
  }

  List<Widget> _getDaysNoUI() {
    List<Widget> numberList = List<Widget>();
    var count = 0;
    for (var iteratorWeekInMonth = 0, weekInMonth = _dateList.length / 7;
        iteratorWeekInMonth < weekInMonth;
        iteratorWeekInMonth++) {
      List<Widget> listNumberDayInWeek = List<Widget>();
      for (var iteratorDayInWeek = 0;
          iteratorDayInWeek < 7;
          iteratorDayInWeek++) {
        final date = _dateList[count];
        listNumberDayInWeek.add(
          Expanded(
            child: AspectRatio(
              aspectRatio: 1.0,
              child: Container(
                child: Stack(
                  children: <Widget>[
                    Material(
                      color: Colors.transparent,
                      child: Container(
                        padding: EdgeInsets.all(7),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: _setRadiusColorDaySelected(date),
                        ),
                        child: FlatButton(
                          padding: EdgeInsets.all(1),
                          onPressed: () => _onSelectDate(date),
                          color: _setBgDaySelected(date),
                          shape: CircleBorder(),
                          child: Center(
                            child: Text(
                              '${date.day}',
                              style: TextStyle(
                                color: _setColorTextPick(date),
                                fontSize:
                                    MediaQuery.of(context).size.width > 360
                                        ? 12
                                        : 10,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 9,
                      right: 0,
                      left: 0,
                      child: Container(
                        height: 6,
                        width: 6,
                        decoration: BoxDecoration(
                          color: _setColorPoint(date),
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
        count += 1;
      }
      numberList.add(Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30)),
          color: _isSameWeek(_indexWeekInMonth, iteratorWeekInMonth)
              ? Color.fromRGBO(26, 41, 116, 1)
              : Colors.transparent,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: listNumberDayInWeek,
        ),
      ));
    }
    return numberList;
  }
}
