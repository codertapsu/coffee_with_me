import 'package:coffee_with_me/shared/styles/style_config.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class SignInForm extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  SignInForm({Key key, @required this.formKey}) : super(key: key);

  _SignInFormState createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {
  String _email;
  String _password;
  @override
  Widget build(BuildContext context) {
    final Widget _emailField = TextFormField(
      decoration: InputDecoration(
        labelText: 'Email...',
        hintText: 'marcus@email.com',
        labelStyle: TextStyle(color: Colors.white),
        prefixIcon: Icon(
          LineIcons.envelope,
          color: Colors.white,
        ),
        focusedBorder: StyleConfig.underLineInputStyle,
      ),
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(color: Colors.white),
      cursorColor: Colors.white,
      validator: (value) => value.isEmpty ? 'Email can not be empty' : null,
      onSaved: (value) => _email = value.trim(),
    );
    final Widget _passwordField = TextFormField(
      decoration: InputDecoration(
        labelText: 'Password...',
        labelStyle: TextStyle(color: Colors.white),
        prefixIcon: Icon(
          LineIcons.key,
          color: Colors.white,
        ),
        focusedBorder: StyleConfig.underLineInputStyle,
      ),
      keyboardType: TextInputType.text,
      style: TextStyle(color: Colors.white),
      obscureText: true,
      validator: (value) => value.isEmpty ? 'Password can not be empty' : null,
      onSaved: (value) => _password = value.trim(),
    );
    return Container(
      child: Form(
        key: widget.formKey,
        child: Column(
          children: <Widget>[
            _emailField,
            _passwordField,
          ],
        ),
      ),
    );
  }
}
