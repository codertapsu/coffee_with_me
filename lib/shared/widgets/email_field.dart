import 'package:coffee_with_me/bloc/auth_bloc.dart';
import 'package:coffee_with_me/shared/styles/style_config.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

Widget emailField(BuildContext context) {
  final AuthBloc authBloc = Provider.of(context);
  return StreamBuilder<Object>(
    stream: authBloc.email,
    builder: (BuildContext context, snapshot) {
      return TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          focusedBorder: StyleConfig.underLineInputStyle,
          labelText: 'email',
          labelStyle: TextStyle(color: Colors.white),
          errorText: snapshot.error,
          prefixIcon: Icon(
            LineIcons.envelope,
            color: Colors.white,
          ),
        ),
        style: TextStyle(color: Colors.white),
        onChanged: authBloc.emailChanges,
      );
    },
  );
}
