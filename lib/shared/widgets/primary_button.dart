import 'package:flutter/material.dart';

class PrimaryBtn extends StatelessWidget {
  final VoidCallback onPressCb;
  final String titleBtn;
  const PrimaryBtn({
    Key key,
    @required this.onPressCb,
    @required this.titleBtn,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        color: Colors.white,
      ),
      child: RaisedButton(
        onPressed: onPressCb,
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
        child: Text(
          '$titleBtn',
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 20,
          ),
        ),
      ),
    );
  }
}
