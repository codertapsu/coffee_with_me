import 'package:coffee_with_me/shared/styles/style_config.dart';
import 'package:flutter/material.dart';

Widget inputField(_password, String labelText, IconData icon,
    {TextInputType keyboardType: TextInputType.text,
    bool obscureText: false,
    bool validate: true,
    String validateText: 'This field is required'}) {
  return TextFormField(
    decoration: InputDecoration(
      labelText: labelText,
      labelStyle: TextStyle(color: Colors.white),
      prefixIcon: Icon(
        icon,
        color: Colors.white,
      ),
      focusedBorder: StyleConfig.underLineInputStyle,
    ),
    keyboardType: keyboardType,
    style: TextStyle(color: Colors.white),
    obscureText: obscureText,
    validator: (value) => (validate ?? value.isEmpty) ? validateText : null,
    onSaved: (value) => _password = value.trim(),
  );
}
