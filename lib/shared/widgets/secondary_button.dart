import 'package:flutter/material.dart';

class SecondaryBtn extends StatelessWidget {
  final VoidCallback onPressCb;
  final String titleBtn;
  const SecondaryBtn({
    Key key,
    @required this.onPressCb,
    @required this.titleBtn,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressCb,
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            border: Border.all(color: Colors.white),
            color: Colors.transparent),
        child: Center(
          child: Text(
            '$titleBtn',
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 20,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
