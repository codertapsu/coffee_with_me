import 'package:coffee_with_me/bloc/auth_bloc.dart';
import 'package:coffee_with_me/shared/styles/style_config.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';

Widget firstNameField(BuildContext context) {
  final authBloc = Provider.of<AuthBloc>(context);
  return StreamBuilder<Object>(
    stream: authBloc.firstName,
    builder: (BuildContext context, snapshot) {
      return TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          focusedBorder: StyleConfig.underLineInputStyle,
          labelText: 'first name',
          labelStyle: TextStyle(color: Colors.white),
          errorText: snapshot.error,
          prefixIcon: Icon(
            LineIcons.pencil,
            color: Colors.white,
          ),
        ),
        style: TextStyle(color: Colors.white),
        onChanged: authBloc.firstNameChanges,
      );
    },
  );
}
