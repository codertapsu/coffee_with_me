import 'package:coffee_with_me/shared/styles/style_config.dart';
import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final Function callback;
  final double paddingTop;
  final double paddingRight;
  final double paddingBottom;
  final double paddingLeft;
  final double borderRadiusTopLeft;
  final double borderRadiusTopRight;
  final double borderRadiusBottomRight;
  final double borderRadiusBottomLeft;
  final String labelText;
  final TextInputType keyboardType;
  final bool isSecureText;
  final bool isReadonly;

  const TextFieldWidget({
    Key key,
    this.callback,
    this.paddingTop,
    this.paddingRight,
    this.paddingBottom,
    this.paddingLeft,
    this.borderRadiusTopLeft,
    this.borderRadiusTopRight,
    this.borderRadiusBottomRight,
    this.borderRadiusBottomLeft,
    this.labelText,
    this.keyboardType,
    this.isSecureText,
    this.isReadonly,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        top: paddingTop,
        right: paddingRight,
        bottom: paddingBottom,
        left: paddingLeft,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 40,
        child: Material(
          elevation: 10,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(borderRadiusTopLeft),
              topRight: Radius.circular(borderRadiusTopRight),
              bottomRight: Radius.circular(borderRadiusBottomRight),
              bottomLeft: Radius.circular(borderRadiusBottomLeft),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.all(10),
            child: TextField(
              decoration: InputDecoration(
                border: InputBorder.none,
                labelText: '$labelText',
                labelStyle: TextStyle(color: Colors.white),
                focusedBorder: StyleConfig.underLineInputStyle,
              ),
              keyboardType: keyboardType,
              style: TextStyle(color: Colors.white),
              obscureText: isSecureText,
              readOnly: isReadonly,
              onChanged: (content) => callback(content),
            ),
          ),
        ),
      ),
    );
  }
}
