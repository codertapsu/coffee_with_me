import 'package:flutter/material.dart';

class RoundRectBtn extends StatelessWidget {
  final String title;
  final List<Color> bgGradient;
  final Color color;
  final BorderSide border;
  final double width;
  final BorderRadiusGeometry borderRadius;
  final double padBottom;
  final bool isEndIconVisible;
  final String pathIcon;
  final void Function(int) onTapCb;
  RoundRectBtn({
    Key key,
    this.title,
    this.bgGradient,
    this.color,
    this.border,
    this.width,
    this.borderRadius,
    this.padBottom,
    this.isEndIconVisible,
    this.pathIcon,
    this.onTapCb,
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 0,
      focusElevation: 0,
      hoverElevation: 0,
      highlightElevation: 0,
      disabledElevation: 0,
      onPressed: () {
        onTapCb(1);
      },
      color: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
      child: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 20,
        ),
      ),
      // child: Padding(
      //   padding: EdgeInsets.only(bottom: padBottom),
      //   child: Stack(
      //     alignment: Alignment(1, 0),
      //     children: <Widget>[
      //       Container(
      //         alignment: Alignment.center,
      //         width: width,
      //         decoration: ShapeDecoration(
      //           shape: RoundedRectangleBorder(
      //             borderRadius: borderRadius,
      //             side: border,
      //           ),
      //           gradient: LinearGradient(
      //             colors: bgGradient,
      //             begin: Alignment.topCenter,
      //             end: Alignment.bottomCenter,
      //           ),
      //         ),
      //         child: Text(
      //           title,
      //           style: TextStyle(
      //             color: color,
      //             fontSize: 18,
      //             fontWeight: FontWeight.w500,
      //           ),
      //         ),
      //         padding: EdgeInsets.only(
      //           top: 16,
      //           bottom: 16,
      //         ),
      //       ),
      //       Visibility(
      //         visible: isEndIconVisible,
      //         child: Padding(
      //           padding: EdgeInsets.only(right: 10),
      //           child: ImageIcon(
      //             AssetImage(pathIcon),
      //             size: 30,
      //             color: color,
      //           ),
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }
}
