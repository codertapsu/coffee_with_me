import 'package:flutter/material.dart';

class FancyBottomNavigationItem {
  final Icon icon;
  final Text title;
  FancyBottomNavigationItem({
    @required this.icon,
    @required this.title,
  }) {
    assert(icon != null);
    assert(title != null);
  }
}
