class Constants {
  static const String fireBaseUrl = 'gs://coffee-with-me.appspot.com';
  static const String errorPhone = 'Phone is incorrect';
  static const String errorEmail = 'Email is incorect';
  static const String enterPhone = 'Please enter your phone';
  static const String enterEmail = 'Please enter your email';
  static const String usePhone = 'Use phone instead';
  static const String useEmail = 'Use email instead';
  static const String submit = 'submit';
  static const String labelPhone = 'Phone';
  static const String labelEmail = 'Email';
  static const String sentEmail = 'Check your email for the confirmation link';
  static const String resetedPassword = 'Check your email for the reset password';
  static const String forgotPassword = 'Remind my password';
  static const String error = 'Please check the input field';
  static const String verificationFailed = 'Verification failed';
  static const String thankYou = 'Thank you for authenticating!';
  static const String success = 'Success';
  static const String confirm = 'Confirm';
  static const String cancel = 'Cancel';
  static const int timeOutDuration = 90;

  static const String user_avatar_key = 'user-avatar';
  static const String authToken = 'firebase-token';
  static const String default_avatar_path = 'assets/images/man1.jpg';

  static const String hello_new_guy = 'Welcome to Coffee with me';

  static const String apply_text = 'Apply';
  static const String format_day_name_full = 'EEEEEE';
  static const String format_day_name_short = 'EEE';
  static const String format_month_long = 'MMMM';
  static const String format_month_short = 'MMM';
  static const String format_year_long = 'yyyy';
  static const String format_time_long = 'MMMM, yyyy';
  static const String format_time_short = 'MMM, yyyy';
}
