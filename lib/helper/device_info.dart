import 'dart:io';
import 'package:device_info/device_info.dart';

Future<dynamic> deviceInfo() async {
  DeviceInfoPlugin _devicePlugin = new DeviceInfoPlugin();
  if (Platform.isIOS) {
    return await _devicePlugin.iosInfo;
  } else {
    return await _devicePlugin.androidInfo;
  }
}
