import 'dart:async';

import 'package:coffee_with_me/shared/constant/constant.config.dart';

class Validator {
  final validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    if (RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email)) {
      sink.add(email);
    } else {
      sink.addError(Constants.errorEmail);
    }
  });

    final validatePassword =
      StreamTransformer<String, String>.fromHandlers(handleData: (password, sink) {
    if (RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(password)) {
      sink.add(password);
    } else {
      sink.addError(Constants.errorEmail);
    }
  });

  final validatePhone =
      StreamTransformer<String, String>.fromHandlers(handleData: (phone, sink) {
    if (RegExp(r"^(?:[+0]+)?[0-9]{6,14}$").hasMatch(phone)) {
      sink.add(phone);
    } else {
      sink.addError(Constants.errorPhone);
    }
  });
}
