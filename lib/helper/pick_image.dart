import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

void openImagePickerModal(BuildContext context, Function(ImageSource) callback) {
  final flatButtonColor = Theme.of(context).primaryColor;
  showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 150,
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Text(
                'Pick a image',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              FlatButton(
                textColor: flatButtonColor,
                child: Text('Use Camera'),
                onPressed: (){
                  callback(ImageSource.camera);
                },
              ),
              FlatButton(
                textColor: flatButtonColor,
                child: Text('Use Gallery'),
                onPressed: (){
                  callback(ImageSource.gallery);
                },
              ),
            ],
          ),
        );
      });
}
