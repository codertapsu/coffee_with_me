import 'package:flutter/material.dart';
import 'package:native_widgets/native_widgets.dart';

void displayDialog(BuildContext context, String title, String message) {
  void _displayPopup({BuildContext context, Widget child}) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) => child,
    );
  }

  // showDialog(
  //   context: context,
  //   builder: (BuildContext context) {
  //     return AlertDialog(content: Text('$message'));
  //   });
  return _displayPopup(
    context: context,
    child: NativeDialog(
      title: Text(title),
      content: Text(message),
      actions: <NativeDialogAction>[
        NativeDialogAction(
          text: Text('ok'),
          isDestructive: false,
          onPressed: () => Navigator.pop(context),
        ),
      ],
    ),
  );
}
