import 'dart:io';
import 'package:coffee_with_me/models/request.http.dart';
import 'package:coffee_with_me/services/local_storage.service.dart';
import 'package:coffee_with_me/shared/constant/constant.config.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class HttpService {
  final LocalStorageService _localStorageService = new LocalStorageService();
  final Dio _dio = new Dio();
  final _endPoint = DotEnv().env['API_ENDPOINT'];
  String _authorization;
  HttpService() {
    _localStorageService.getValue(Constants.authToken).then((value) {
      _authorization = value;
    });
    this._dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          options.baseUrl = _endPoint;
          options.headers = {
            'Content-Type': 'application/json',
            'Authorization': _authorization
          };
          return options;
        },
        // onResponse: _handleResponse,
        // onError: _handleErr,
      ),
    );
  }
  // static RequestResponse _handleResponse(Response response) {
  //   return RequestResponse(
  //     response.data,
  //     response.statusCode,
  //     response.statusMessage,
  //   );
  // }

  // static RequestError _handleErr(DioError error) {
  //   throw RequestError(error.message, error.type.toString());
  // }

  Future<Response> post(String path, Object data) async {
    Response response = await _dio.post(path, data: data);
    return response;
    // return _dio.post(path, data: data);
  }

  Future<Response> get(String path,
      {Map<String, dynamic> queryParameters = const {}}) async {
    return await _dio.get(path);
  }

  Future<dynamic> postFormData() async {
    FormData formData = new FormData.from({
      'name': 'wendux',
      'age': 25,
      'file1': new UploadFileInfo(new File('./upload.txt'), 'upload1.txt'),
      'files': [
        new UploadFileInfo(new File('./example/upload.txt'), 'upload.txt'),
        new UploadFileInfo(new File('./example/upload.txt'), 'upload.txt')
      ]
    });
    return _dio.post('/info', data: formData,
        onSendProgress: (int sent, int total) {
      print('$sent/$total');
    });
  }

  Future<Response> postImage(File image, String imageName) async {
    FormData formData = new FormData.from({
      'image': new UploadFileInfo(image, imageName),
    });
    Options options = new Options();
    options.headers = {'Authorization': _authorization};
    return _dio.post('http://192.168.1.176:8080/api/media/images',
        data: formData, options: options);
  }
}
