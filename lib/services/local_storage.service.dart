// import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LocalStorageService {
  final FlutterSecureStorage _storage = new FlutterSecureStorage();
  Future<void> setValue(String key, dynamic value) async {
    return await _storage.write(key: key, value: value.toString());
  }

  Future<String> getValue(String key) async {
    return await _storage.read(key: key);
  }

  Future<Map<String, String>> getAllValues() async {
    return await _storage.readAll();
  }

  Future<void> removeAllValues() async {
    return await _storage.deleteAll();
  }

  Future<void> removeValue(String key) async {
    return await _storage.delete(key: key);
  }
}
