import 'dart:io';

import 'package:coffee_with_me/services/local_storage.service.dart';
import 'package:coffee_with_me/shared/constant/constant.config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';

abstract class FirebaseAuthBase {
  Future<FirebaseUser> signIn(String email, String password);
  Future<FirebaseUser> signUp(
    String firstName,
    String lastName,
    String email,
    String password,
  );
  Future<FirebaseUser> getCurrentUser();
  Future<void> updateCurrentUser(FirebaseUser user, UserUpdateInfo info);
  Future<void> sendEmailVerification();
  Future<void> signOut();
  Future<bool> isEmailVerifired();
  Future<void> resetPassword(String email);
}

class FireBaseAuthService with ChangeNotifier implements FirebaseAuthBase {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final LocalStorageService _localStorageService = LocalStorageService();

  Future<FirebaseUser> signIn(
    String email,
    String password,
  ) async {
    try {
      AuthResult authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      notifyListeners();
      await saveIdToken(authResult.user);
      return authResult.user;
    } on AuthException catch (e) {
      throw new AuthException(e.code, e.message);
    }
  }

  Future<FirebaseUser> signUp(
      String firstName, String lastName, String email, String password,
      {File avatar}) async {
    try {
      AuthResult authResult =
          await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      UserUpdateInfo info = new UserUpdateInfo();
      info.displayName = '$firstName $lastName';
      await authResult.user.updateProfile(info);
      // await authResult.user.sendEmailVerification();
      await saveIdToken(authResult.user);
      return authResult.user;
    } on AuthException catch (e) {
      throw new AuthException(e.code, e.message);
    }
  }

  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser _currentUser = await _firebaseAuth.currentUser();
    await saveIdToken(_currentUser);
    return _currentUser;
  }

  Future<void> updateCurrentUser(FirebaseUser user, UserUpdateInfo info) async {
    return user.updateProfile(info);
  }

  Future<void> sendEmailVerification() async {
    FirebaseUser authResult = await _firebaseAuth.currentUser();
    return authResult.sendEmailVerification();
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<bool> isEmailVerifired() async {
    FirebaseUser authResult = await _firebaseAuth.currentUser();
    return authResult.isEmailVerified;
  }

  Future<void> resetPassword(String email) async {
    return _firebaseAuth.sendPasswordResetEmail(email: email);
  }

  Future<void> saveIdToken(FirebaseUser currentUser) async {
    if (currentUser?.uid != null) {
      final IdTokenResult idToken = await currentUser.getIdToken();
      _localStorageService.setValue(
          Constants.authToken, idToken.token.toString());
    }
  }
}
