import 'package:flutter/material.dart';

class NavigationService {
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName, {dynamic args}) {
    return navigatorKey.currentState.pushNamed(routeName, arguments: args);
  }

  Future<dynamic> signOut() {
    return navigatorKey.currentState.pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  bool goBack() {
    return navigatorKey.currentState.pop();
  }
}
