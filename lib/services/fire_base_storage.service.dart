import 'dart:io';
import 'package:path/path.dart' as path;

import 'package:firebase_storage/firebase_storage.dart';

abstract class FirebaseStorageBase {
  Future<String> uploadImage(File image);
}

class FirebaseStorageService implements FirebaseStorageBase {
  final StorageReference _firebaseStorage =
      FirebaseStorage(storageBucket: 'gs://coffee-with-me.appspot.com').ref();
  Future<String> uploadImage(File image) async {
    String ext = path.extension(image.path);
    StorageTaskSnapshot uploadImage = await _firebaseStorage
        .child('avatars')
        .child('${DateTime.now().toUtc().toString()}$ext')
        .putFile(image)
        .onComplete;
    final dynamic link = await uploadImage.ref.getDownloadURL();
    return link.toString();
  }
}
