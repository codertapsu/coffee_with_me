import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastLocal {
  void showTop(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.TOP,
        fontSize: 14.0,
        backgroundColor: Colors.white,
        textColor: Colors.black);
  }

  void clearToast() {
    Fluttertoast.cancel();
  }
}
