import 'package:coffee_with_me/services/stop-able.service.dart';

class BackgroundService extends StoppableService {
  @override
  void start() {
    super.start();
    print('BackgroundService Start');
  }

  @override
  void stop() {
    super.stop();
    print('BackgroundService Stop');
  }
}
