import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/error_codes.dart' as auth_error;

class LocalAuthService {
  final LocalAuthentication _auth = LocalAuthentication();

  final IOSAuthMessages _iosStrings = IOSAuthMessages(
    cancelButton: 'cancel',
    goToSettingsButton: 'settings',
    goToSettingsDescription: 'Please set up your Touch ID.',
    lockOut: 'Please reenable your Touch ID',
  );
  final AndroidAuthMessages _androidAuthMessages = AndroidAuthMessages(
    fingerprintHint: 'fingerprintHint',
    fingerprintNotRecognized: 'fingerprintNotRecognized',
    fingerprintSuccess: 'fingerprintSuccess',
    cancelButton: 'cancelButton',
    signInTitle: 'signInTitle',
    fingerprintRequiredTitle: 'fingerprintRequiredTitle',
    goToSettingsButton: 'goToSettingsButton',
    goToSettingsDescription: 'goToSettingsDescription',
  );

  bool _isProtectionEnabled = true;
  bool get isProtectionEnabled => _isProtectionEnabled;
  set isProtectionEnabled(bool isEnable) => _isProtectionEnabled = isEnable;

  bool isAuthenticated = false;

  Future<bool> canCheckBiometrics() {
    return _auth.canCheckBiometrics;
  }

  Future<List<BiometricType>> availableBiometrics() {
    return _auth.getAvailableBiometrics();
  }

  Future<bool> authenticate() {
    return _auth.authenticateWithBiometrics(
      localizedReason: 'Authenticate to access',
      useErrorDialogs: true,
      stickyAuth: false,
    );
  }

}
