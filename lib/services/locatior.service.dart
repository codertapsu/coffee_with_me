import 'package:get_it/get_it.dart';
import 'package:coffee_with_me/services/local_auth.service.dart';
import 'package:coffee_with_me/services/navigation.service.dart';
import 'package:coffee_with_me/services/background.service.dart';

GetIt locator = GetIt.I;
void setUpLocator() {
  locator.registerSingleton<LocalAuthService>(new LocalAuthService());
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
  locator.registerLazySingleton<BackgroundService>(() => BackgroundService());
}
