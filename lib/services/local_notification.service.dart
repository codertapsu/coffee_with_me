import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:meta/meta.dart';

class LocalNotification {
  static final _plugin = FlutterLocalNotificationsPlugin();
  final settingsAndroid = AndroidInitializationSettings('app_icon');
  final settingsIOS = IOSInitializationSettings(
      onDidReceiveLocalNotification: (id, title, body, payload) =>
          onSelectNotification(payload));
  static Future onSelectNotification(String payload) async {}
  LocalNotification() {
    _plugin.initialize(
      InitializationSettings(settingsAndroid, settingsIOS),
      onSelectNotification: onSelectNotification,
    );
  }
  NotificationDetails get _noSound {
    final androidChannelSpecifics = AndroidNotificationDetails(
      'silent channel id',
      'silent channel name',
      'silent channel description',
      playSound: false,
    );
    final iOSChannelSpecifics = IOSNotificationDetails(presentSound: false);
    return NotificationDetails(androidChannelSpecifics, iOSChannelSpecifics);
  }

  NotificationDetails get _ongoing {
    final androidChannelSpecifics = AndroidNotificationDetails(
      'your channel id',
      'your channel name',
      'your channel description',
      importance: Importance.Max,
      priority: Priority.High,
      ongoing: true,
      autoCancel: false,
    );
    final iOSChannelSpecifics = IOSNotificationDetails();
    return NotificationDetails(androidChannelSpecifics, iOSChannelSpecifics);
  }

  Future<void> _showNotification({
    @required String title,
    @required String body,
    @required NotificationDetails type,
    int id = 0,
  }) {
    return _plugin.show(id, title, body, type);
  }

  Future<void> showSilentNotification({
    @required String title,
    @required String body,
    int id = 0,
  }) {
    return _showNotification(
      title: title,
      body: body,
      id: id,
      type: _noSound,
    );
  }

  Future showOngoingNotification({
    @required String title,
    @required String body,
    int id = 0,
  }) {
    return _showNotification(
      title: title,
      body: body,
      id: id,
      type: _ongoing,
    );
  }
}
