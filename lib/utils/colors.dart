import 'package:flutter/material.dart';
import 'dart:ui';

_hexToColor(String code) {
  return int.parse(code.substring(1, 7), radix: 16) + 0xFF000000;
}

const primaryDark = const Color(0xFFFF9F59);
const primaryColor = const Color(0xFFfbab66);

const Color gradientStart = const Color(0xFFfbab66);
const Color gradientEnd = const Color(0xFFf7418c);

const Color lightPink = Color(0xffFC5CF0);
const Color darkOrange = Color(0xffFE8852);

const primaryGradient = const LinearGradient(
  colors: const [gradientStart, gradientEnd],
  begin: Alignment.topCenter,
  end: Alignment.bottomCenter,
);
final negaGradient = LinearGradient(
  colors: [_hexToColor('#ee9ca7'), _hexToColor('#ffdde1')],
  begin: Alignment.bottomCenter,
  end: Alignment.topCenter,
);

const List<Color> skyGradients = [
  Color(0xFF0EDED2),
  Color(0xFF03A0FE),
];

const List<Color> orangeGradients = [
  Color(0xFFFF9945),
  Color(0xFFFc6076),
];
