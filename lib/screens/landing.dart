import 'package:coffee_with_me/routing/routes.dart' as routes;
import 'package:coffee_with_me/services/fire_base_auth.service.dart';
import 'package:coffee_with_me/services/locatior.service.dart';
import 'package:coffee_with_me/services/navigation.service.dart';
import 'package:flutter/material.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key key}) : super(key: key);

  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  final NavigationService _navigationService = locator<NavigationService>();
  final FireBaseAuthService _firebaseAuthBase = new FireBaseAuthService();

  @override
  void initState() {
    super.initState();
    _firebaseAuthBase.getCurrentUser().then((currentUser) {
      setState(() {
        if (currentUser?.uid != null) {
          _navigationService.navigateTo(routes.HomeRoute);
        } else {
          _navigationService.navigateTo(routes.SignRoute);
        }
      });
    }).catchError((error) {
      print('Error: ${error.toString()}');
      _navigationService.navigateTo(routes.SignRoute);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ),
    );
  }
}
