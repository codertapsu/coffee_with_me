import 'package:coffee_with_me/screens/signIn.dart';
import 'package:coffee_with_me/screens/signUp.dart';
import 'package:coffee_with_me/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

enum FormMode { LOGIN, SIGNUP }

class SignPage extends StatefulWidget {
  const SignPage({Key key}) : super(key: key);

  _SignPageState createState() => _SignPageState();
}

class _SignPageState extends State<SignPage> {
  PageController _pageController = PageController(initialPage: 0);
  void _animateSlider(int pageIndex) {
    _pageController.animateToPage(pageIndex,
        duration: Duration(milliseconds: 500), curve: Curves.linear);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: primaryDark),
    );
    final PageView _pageView = PageView(
      physics: BouncingScrollPhysics(),
      controller: _pageController,
      children: <Widget>[
        SignInPage(callback: () => _animateSlider(1)),
        SignUppage(callback: () => _animateSlider(0)),
      ],
    );
    return Scaffold(
      body: _pageView,
    );
  }
}
