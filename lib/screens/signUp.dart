import 'dart:io';
import 'package:coffee_with_me/helper/dialog.dart';
import 'package:coffee_with_me/helper/pick_image.dart';
import 'package:coffee_with_me/routing/routes.dart' as routes;
import 'package:coffee_with_me/services/fire_base_auth.service.dart';
import 'package:coffee_with_me/services/local_notification.service.dart';
import 'package:coffee_with_me/services/local_storage.service.dart';
import 'package:coffee_with_me/services/locatior.service.dart';
import 'package:coffee_with_me/services/navigation.service.dart';
import 'package:coffee_with_me/shared/constant/constant.config.dart';
import 'package:coffee_with_me/shared/styles/style_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:coffee_with_me/shared/widgets/primary_button.dart';
import 'package:coffee_with_me/utils/colors.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_icons/line_icons.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class SignUppage extends StatefulWidget {
  final VoidCallback callback;
  final FireBaseAuthService auth = new FireBaseAuthService();
  SignUppage({this.callback});
  _SignUppageState createState() => _SignUppageState();
}

class _SignUppageState extends State<SignUppage> {
  final NavigationService _navigationService = locator<NavigationService>();
  final LocalStorageService _localStorageService = LocalStorageService();

  final LocalNotification _localNotification = LocalNotification();

  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;
  String _firstName;
  String _lastName;
  File _imageProfile;

  String _errMessage;
  bool _isLoading;
  bool _isIos;

  bool _validateAndSave() {
    final FormState _formState = _formKey.currentState;
    if (_formState.validate()) {
      _formState.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit(BuildContext currentContext) async {
    setState(() {
      _errMessage = '';
      _isLoading = true;
    });
    if (_validateAndSave()) {
      FirebaseUser _user;
      try {
        _user =
            await widget.auth.signUp(_firstName, _lastName, _email, _password);
        setState(() {
          _isLoading = false;
        });
        final Directory directory = await getApplicationDocumentsDirectory();
        final String imageName = basename(_imageProfile.path);
        final File localImage =
            await _imageProfile.copy('${directory.path}/$imageName');
        _localStorageService.setValue(
          '${Constants.user_avatar_key}-${_user.uid}',
          localImage.path,
        );
        _localNotification.showSilentNotification(title: Constants.hello_new_guy, body: '$_firstName $_lastName');
        _navigationService.navigateTo(routes.HomeRoute);
      } catch (error) {
        setState(() {
          _isLoading = false;
          _errMessage = _isIos ? error.detail : error.message;
        });
        displayDialog(currentContext, 'Sign up', _errMessage);
      }
    }
  }

  void _getImage(BuildContext context, ImageSource source) async {
    File image = await ImagePicker.pickImage(source: source);
    setState(() {
      _imageProfile = image;
    });
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _isIos = Theme.of(context).platform == TargetPlatform.iOS;
    final Widget pageTitle = Center(
      child: Padding(
        padding: EdgeInsets.only(left: 30),
        child: InkWell(
          onTap: () => openImagePickerModal(context, (source) {
            _getImage(context, source);
          }),
          child: _imageProfile == null
              ? Container(
                  height: 120,
                  width: 120,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white),
                    shape: BoxShape.circle,
                  ),
                  child: Icon(Icons.add_a_photo, size: 25, color: Colors.white),
                )
              : Container(
                  height: 120,
                  width: 120,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: ExactAssetImage(_imageProfile.path),
                        fit: BoxFit.cover),
                    shape: BoxShape.circle,
                  ),
                ),
        ),
      ),
    );
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 150, right: 30),
          decoration: BoxDecoration(gradient: primaryGradient),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              pageTitle,
              Container(
                padding: EdgeInsets.only(left: 30),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'last name',
                          labelStyle: TextStyle(color: Colors.white),
                          prefixIcon: Icon(
                            LineIcons.pencil,
                            color: Colors.white,
                          ),
                          focusedBorder: StyleConfig.underLineInputStyle,
                        ),
                        keyboardType: TextInputType.text,
                        style: TextStyle(color: Colors.white),
                        validator: (value) =>
                            value.isEmpty ? 'please type your last name' : null,
                        onSaved: (value) => _lastName = value.trim(),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'first name',
                          labelStyle: TextStyle(color: Colors.white),
                          prefixIcon: Icon(
                            LineIcons.pencil,
                            color: Colors.white,
                          ),
                          focusedBorder: StyleConfig.underLineInputStyle,
                        ),
                        keyboardType: TextInputType.text,
                        style: TextStyle(color: Colors.white),
                        validator: (value) => value.isEmpty
                            ? 'please type your first name'
                            : null,
                        onSaved: (value) => _firstName = value.trim(),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'email',
                          labelStyle: TextStyle(color: Colors.white),
                          prefixIcon: Icon(
                            LineIcons.envelope,
                            color: Colors.white,
                          ),
                          focusedBorder: StyleConfig.underLineInputStyle,
                        ),
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: Colors.white),
                        validator: (value) =>
                            value.isEmpty ? 'please type your email' : null,
                        onSaved: (value) => _email = value.trim(),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'password',
                          labelStyle: TextStyle(color: Colors.white),
                          prefixIcon: Icon(
                            LineIcons.key,
                            color: Colors.white,
                          ),
                          focusedBorder: StyleConfig.underLineInputStyle,
                        ),
                        keyboardType: TextInputType.text,
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        validator: (value) =>
                            value.isEmpty ? 'please type your password' : null,
                        onSaved: (value) => _password = value.trim(),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 30),
              Container(
                padding: EdgeInsets.only(left: 30),
                child: new PrimaryBtn(
                  onPressCb: () => _validateAndSubmit(context),
                  titleBtn: 'sign up',
                ),
              ),
              SizedBox(height: 30),
              Align(
                alignment: Alignment.centerLeft,
                child: RaisedButton(
                  padding: EdgeInsets.fromLTRB(40, 16, 30, 16),
                  color: Colors.white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(30),
                      bottomRight: Radius.circular(30),
                    ),
                  ),
                  onPressed: widget.callback,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(LineIcons.arrow_left, size: 18),
                      SizedBox(width: 10),
                      Text(
                        'Sign in'.toUpperCase(),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
