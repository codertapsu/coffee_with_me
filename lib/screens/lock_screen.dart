import 'package:coffee_with_me/services/local_auth.service.dart';
import 'package:coffee_with_me/services/locatior.service.dart';
import 'package:coffee_with_me/services/navigation.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:coffee_with_me/routing/routes.dart' as routes;
import 'package:coffee_with_me/utils/colors.dart';
import 'package:local_auth/local_auth.dart';

class LockScreen extends StatefulWidget {
  _LockScreenState createState() => _LockScreenState();
}

class _LockScreenState extends State<LockScreen> {
  final NavigationService _navigationService = locator<NavigationService>();
  final LocalAuthService localAuth = new LocalAuthService();
  bool _canUseBiometrics = false;

  final List<List<int>> _passcodeNumber = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
  ];
  String _passcode = '';

  @override
  void initState() {
    super.initState();
  }

  void _navigateToHome() {
    _navigationService.navigateTo(routes.HomeRoute);
  }

  void _checkBiometrics() async {
    bool isCanUseBio = await localAuth.canCheckBiometrics();
    List<BiometricType> bioTypes = await localAuth.availableBiometrics();
    if (isCanUseBio && bioTypes.contains(BiometricType.fingerprint)) {
      setState(() {
        _canUseBiometrics = isCanUseBio;
      });
    }
  }

  void _onScanFinger() async {
    try {
      bool isAuthenticated = await localAuth.authenticate();
      if (isAuthenticated) {
        _navigateToHome();
      }
    } on PlatformException catch (e) {
      print(e);
    }
  }

  void _setPasscode(int value) {
    if (_passcode.length < 6) {
      setState(() {
        _passcode += '$value';
        if (_passcode == '123123') {
          _navigateToHome();
        }
      });
    }
  }

  void _onDeletePasscode() {
    setState(() {
      _passcode = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    _checkBiometrics();
    Widget _number(int value) {
      return RaisedButton(
        elevation: 0,
        focusElevation: 0,
        hoverElevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        padding: EdgeInsets.all(17),
        child: Text(
          '$value',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => _setPasscode(value),
        shape: CircleBorder(
          side: BorderSide(
            color: Colors.white,
          ),
        ),
        color: Colors.transparent,
      );
    }

    final Widget _passCodeBoard = Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: Column(
        children: <Widget>[
          Column(
            children: _passcodeNumber
                .map((rowItem) => Column(
                      children: <Widget>[
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children:
                              rowItem.map((item) => _number(item)).toList(),
                        ),
                        SizedBox(height: 15),
                      ],
                    ))
                .toList(),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _number(0),
              RaisedButton(
                elevation: 0,
                focusElevation: 0,
                hoverElevation: 0,
                highlightElevation: 0,
                disabledElevation: 0,
                padding: EdgeInsets.all(17),
                child: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                  size: 26,
                ),
                onPressed: _onDeletePasscode,
                shape: CircleBorder(
                  side: BorderSide(
                    color: Colors.white,
                  ),
                ),
                color: Colors.transparent,
              )
            ],
          )
        ],
      ),
    );
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(gradient: primaryGradient),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width / 1.5,
              child: Text(
                'Place your Finger on your phone',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
            ),
            SizedBox(height: 25),
            Container(
              width: MediaQuery.of(context).size.width / 1.5,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.circular(7),
              ),
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [1, 2, 3, 4, 5, 6].map((char) {
                  return Icon(
                    _passcode.length >= char ? Icons.star : Icons.star_border,
                    color: Colors.white,
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: 25),
            _passCodeBoard,
            SizedBox(height: 25),
            Visibility(
              visible: _canUseBiometrics,
              child: RaisedButton(
                elevation: 0,
                focusElevation: 0,
                hoverElevation: 0,
                highlightElevation: 0,
                disabledElevation: 0,
                padding: EdgeInsets.all(17),
                onPressed: _onScanFinger,
                child: Text(
                  'Use Finger to unlock',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7),
                  side: BorderSide(color: Colors.white),
                ),
                color: Colors.transparent,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
