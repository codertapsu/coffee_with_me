import 'package:coffee_with_me/helper/dialog.dart';
import 'package:coffee_with_me/models/employee.model.dart';
import 'package:coffee_with_me/services/http.service.dart';
import 'package:coffee_with_me/utils/colors.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:line_icons/line_icons.dart';
import 'package:coffee_with_me/screens/detail_profile_sreen.dart';

class BrowsePage extends StatefulWidget {
  @override
  _BrowsePageState createState() => _BrowsePageState();
}

class _BrowsePageState extends State<BrowsePage>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  bool _isLoading = true;
  final HttpService _httpService = new HttpService();
  final SwiperController _swiperController = new SwiperController();

  List<Employee> _githubUser = [];

  void _listenForEmployee() async {
    try {
      final Response _response = await _httpService.get('/users');
      if (mounted) {
        setState(() {
          _githubUser = (_response.data as List)
              .map<Employee>((value) => Employee.fromJson(value))
              .take(20)
              .toList();
          _isLoading = false;
        });
      }
    } on PlatformException catch (e) {
      displayDialog(context, e.code.toString(), e.message.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    _listenForEmployee();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(height: 40),
          Text(
            'People around you',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.transparent,
              padding: EdgeInsets.all(16.0),
              child: _isLoading
                  ? Container(child: Center(child: CircularProgressIndicator()))
                  : _slideImage(_githubUser),
            ),
          ),
          Visibility(
            visible: !_isLoading,
            child: Stack(
              children: _githubUser
                  .asMap()
                  .map((indexOfList, ele) => MapEntry(
                        indexOfList,
                        AnimatedOpacity(
                          opacity: indexOfList == _currentIndex ? 1 : 0,
                          child: _buildSummary(_currentIndex),
                          duration: Duration(seconds: 1),
                        ),
                      ))
                  .values
                  .toList(),
            ),
          ),
          Container(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 5.0, horizontal: 16.0),
                  margin: const EdgeInsets.only(
                      top: 30, left: 20.0, right: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [lightPink, darkOrange],
                      ),
                      borderRadius: BorderRadius.circular(30.0)),
                  child: Row(
                    children: <Widget>[
                      IconButton(
                        color: Colors.white,
                        icon: Icon(LineIcons.user),
                        onPressed: () {},
                      ),
                      IconButton(
                        color: Colors.white,
                        icon: Icon(Icons.location_on),
                        onPressed: () {},
                      ),
                      Spacer(),
                      IconButton(
                        color: Colors.white,
                        icon: Icon(Icons.add),
                        onPressed: () {},
                      ),
                      IconButton(
                        color: Colors.white,
                        icon: Icon(Icons.message),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
                Center(
                  child: FloatingActionButton(
                    child: Icon(
                      Icons.favorite,
                      color: Colors.pink,
                    ),
                    backgroundColor: Colors.white,
                    onPressed: () {},
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _slideImage(List<Employee> dataArray) {
    return Swiper(
      physics: BouncingScrollPhysics(),
      viewportFraction: 0.8,
      controller: _swiperController,
      onTap: (index) => Navigator.push(
        context,
        PageRouteBuilder(
          transitionDuration: Duration(milliseconds: 500),
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              DetailProfilePage(user: index),
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              FadeTransition(opacity: animation, child: child),
        ),
      ),
      onIndexChanged: (index) {
        setState(() {
          _currentIndex = index;
        });
      },
      itemBuilder: (BuildContext context, int index) {
        return ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Image.network(
            dataArray[index].avatarUrl,
            fit: BoxFit.cover,
          ),
        );
      },
      itemWidth: MediaQuery.of(context).size.width / 1.2,
      itemCount: dataArray.length,
      layout: SwiperLayout.STACK,
    );
  }

  Widget _buildSummary(int index) {
    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Text(
            _githubUser[index].login,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                LineIcons.map_marker,
                color: Colors.grey,
              ),
              Text(
                'Danang, Vietnam',
                style: TextStyle(fontSize: 14, color: Colors.grey),
              ),
            ],
          ),
          SizedBox(height: 8),
          Container(
            width: MediaQuery.of(context).size.width / 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    LineIcons.instagram,
                    color: Colors.grey[600],
                    size: 30,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    LineIcons.facebook,
                    color: Colors.grey[600],
                    size: 30,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    LineIcons.twitter,
                    color: Colors.grey[600],
                    size: 30,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
        ],
      ),
    );
  }
}
