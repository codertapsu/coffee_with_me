import 'dart:io';
import 'package:coffee_with_me/helper/pick_image.dart';
import 'package:coffee_with_me/routing/routes.dart';
import 'package:coffee_with_me/services/fire_base_auth.service.dart';
import 'package:coffee_with_me/services/http.service.dart';
import 'package:coffee_with_me/services/local_storage.service.dart';
import 'package:coffee_with_me/services/locatior.service.dart';
import 'package:coffee_with_me/services/navigation.service.dart';
import 'package:coffee_with_me/shared/constant/constant.config.dart';
import 'package:coffee_with_me/shared/widgets/calendar/date-picker.widget.dart';
import 'package:coffee_with_me/utils/colors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_icons/line_icons.dart';
import 'package:path/path.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final NavigationService _navigationService = locator<NavigationService>();

  final FireBaseAuthService _firebaseAuthBase = new FireBaseAuthService();

  final LocalStorageService _localStorageService = new LocalStorageService();

  final HttpService _httpService = new HttpService();

  FirebaseUser _currentUser;

  ImageProvider _imageProvider;

  void _onSignOut() {
    // _firebaseAuthBase.signOut().then((_) {
    //   _navigationService.signOut();
    // });
  }

  void _onLoadAvatarImage() {
    _localStorageService
        .getValue('${Constants.user_avatar_key}-${_currentUser.uid}')
        .then((value) {
      setState(() {
        _imageProvider = FileImage(File(value));
      });
    });
  }

  void _getImage(BuildContext context, ImageSource source) async {
    try {
      File image = await ImagePicker.pickImage(source: source);
      String fileName = basename(image.path);
      var responseUpimage = await _httpService.postImage(image, fileName);
      print('responseUpimage ${responseUpimage.data}');
      Navigator.pop(context);
    } catch (e) {
      print('Error khi upload anh');
    }
  }

  @override
  void initState() {
    super.initState();
    _firebaseAuthBase.getCurrentUser().then((user) {
      setState(() {
        _currentUser = user;
      });
      _onLoadAvatarImage();
    });
  }

  @override
  Widget build(BuildContext context) {
    final hr = Divider();
    final userStats = Positioned(
      bottom: 10.0,
      left: 40.0,
      right: 40.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _buildUserStats('VISITORS', '2305'),
          _buildUserStats('LIKED', '276'),
          _buildUserStats('MATCHED', '51'),
        ],
      ),
    );

    final _userImage = InkWell(
      onTap: () => openImagePickerModal(context, (source) {
        _getImage(context, source);
      }),
      child: Container(
        height: 100.0,
        width: 100.0,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: _imageProvider == null
                ? AssetImage(Constants.default_avatar_path)
                : _imageProvider,
            fit: BoxFit.cover,
          ),
          shape: BoxShape.circle,
        ),
      ),
    );

    final _userNameLocation = Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            _currentUser == null ? '' : _currentUser.displayName,
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.w900,
            ),
          ),
          Text(
            'Danang, Vietnam',
            style: TextStyle(
              color: Colors.grey.withOpacity(0.6),
              fontSize: 20.0,
              fontWeight: FontWeight.w600,
            ),
          ),
        ],
      ),
    );

    final _userInfo = Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0),
          child: Material(
            elevation: 5.0,
            borderRadius: BorderRadius.circular(8.0),
            shadowColor: Colors.white,
            child: Container(
              height: 220.0,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(
                  color: Colors.grey.withOpacity(0.2),
                ),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 20.0, bottom: 20.0),
                child: Row(
                  children: <Widget>[
                    _userImage,
                    SizedBox(width: 10.0),
                    _userNameLocation
                  ],
                ),
              ),
            ),
          ),
        ),
        userStats
      ],
    );

    final _secondCard = Padding(
      padding: EdgeInsets.only(right: 20.0, left: 20.0, bottom: 30.0),
      child: Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(8.0),
        shadowColor: Colors.white,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Column(
            children: <Widget>[
              // _buildIconTile(Icons.settings, Colors.grey, 'Settings'),
              // hr,
              _buildIconTile(LineIcons.sign_out, Colors.grey, 'Sign out',
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => DatePickerPopup(
                        barrierDismissible: true,
                        onApplyClick: () {},
                        onCancelClick: () {},
                      )
                    );
                  })
            ],
          ),
        ),
      ),
    );
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        height: 350.0,
                      ),
                      Container(
                        height: 250.0,
                        decoration: BoxDecoration(gradient: primaryGradient),
                      ),
                      Positioned(top: 100, right: 0, left: 0, child: _userInfo)
                    ],
                  ),
                  _secondCard
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUserStats(String name, String value) {
    return Column(
      children: <Widget>[
        Text(
          name,
          style: TextStyle(
            color: Colors.grey.withOpacity(0.6),
            fontWeight: FontWeight.w600,
            fontSize: 16.0,
          ),
        ),
        Text(
          value,
          style: TextStyle(
            color: Colors.black87,
            fontWeight: FontWeight.w900,
            fontSize: 20.0,
          ),
        ),
      ],
    );
  }

  Widget _buildIconTile(IconData icon, Color color, String title,
      {VoidCallback onTap}) {
    return InkWell(
      onTap: onTap != null ? onTap : null,
      child: ListTile(
        title: Text(
          title,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        leading: Container(
          height: 30.0,
          width: 30.0,
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Center(
            child: Icon(
              icon,
              color: Colors.white,
            ),
          ),
        ),
        trailing: Icon(LineIcons.chevron_circle_right),
      ),
    );
  }
}
