import 'package:coffee_with_me/screens/calendar_screen.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:coffee_with_me/utils/colors.dart';
import 'package:coffee_with_me/shared/widgets/fancy_bottom_navigation_item.dart';
import 'package:coffee_with_me/screens/fancy_bottom_navigation.dart';
import 'package:coffee_with_me/screens/tabs/profle_screen.dart';
import 'package:line_icons/line_icons.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController _pageController = PageController(initialPage: 0);
  StreamController<int> _indexcontroller = StreamController<int>.broadcast();

  List<FancyBottomNavigationItem> _bottomNavigationList = [
    // FancyBottomNavigationItem(
    //   icon: Icon(Icons.notifications),
    //   title: Text('Notification'),
    // ),
    // FancyBottomNavigationItem(
    //   icon: Icon(Icons.search),
    //   title: Text('Find'),
    // ),
    FancyBottomNavigationItem(
      icon: Icon(Icons.person),
      title: Text('Profile'),
    ),
    FancyBottomNavigationItem(
      icon: Icon(LineIcons.calendar),
      title: Text('Calendar'),
    ),
    // FancyBottomNavigationItem(
    //   icon: Icon(Icons.message),
    //   title: Text('Message'),
    // ),
  ];
  final List<Widget> _pages = [
    // NotificationsPage(),
    // BrowsePage(),
    ProfilePage(),
    CalendarScreen(),
    // BrowsePage(),
  ];
  void _animateSlider(int pageIndex) {
    // Future.delayed(Duration(seconds: 2)).then((_) {
    //   int nextPage = _pageController.page.round() + 1;
    //   if (nextPage == _bottomNavigationList.length) {
    //     nextPage = 0;
    //   }
    //   _pageController.animateToPage(nextPage,
    //       duration: Duration(seconds: 1), curve: Curves.linear);
    // });
    _pageController.animateToPage(pageIndex,
        duration: Duration(milliseconds: 500), curve: Curves.linear);
  }

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance
    //     .addPostFrameCallback((callback) => _animateSlider());
  }

  @override
  void dispose() {
    _indexcontroller.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final StreamBuilder<Object> _bottomNavBar = StreamBuilder<Object>(
        initialData: 0,
        stream: _indexcontroller.stream,
        builder: (context, snapshot) {
          int cIndex = snapshot.data;
          return FancyBottomNavigation(
            currentIndex: cIndex,
            items: _bottomNavigationList,
            activeColor: lightPink,
            onItemSelected: (int value) {
              _indexcontroller.add(value);
              _animateSlider(value);
            },
          );
        });
    final PageView _pageView = PageView(
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: (index) {
          _indexcontroller.add(index);
        },
        controller: _pageController,
        children: _pages);
    return Scaffold(
      body: _pageView,
      bottomNavigationBar: _bottomNavBar,
    );
  }
}
