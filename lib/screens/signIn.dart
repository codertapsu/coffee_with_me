import 'package:coffee_with_me/helper/dialog.dart';
import 'package:coffee_with_me/routing/routes.dart' as routes;
import 'package:coffee_with_me/services/fire_base_auth.service.dart';
import 'package:coffee_with_me/services/local_notification.service.dart';
import 'package:coffee_with_me/services/locatior.service.dart';
import 'package:coffee_with_me/services/navigation.service.dart';
import 'package:coffee_with_me/services/toast.dart';
import 'package:coffee_with_me/shared/constant/constant.config.dart';
import 'package:coffee_with_me/shared/styles/style_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:coffee_with_me/shared/widgets/primary_button.dart';
import 'package:coffee_with_me/utils/colors.dart';
import 'package:line_icons/line_icons.dart';

class SignInPage extends StatefulWidget {
  final VoidCallback callback;
  final FireBaseAuthService auth = new FireBaseAuthService();
  SignInPage({this.callback});
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final NavigationService _navigationService = locator<NavigationService>();
  final TextEditingController _textFieldController = TextEditingController();

  final ToastLocal _toastLocal = ToastLocal();

  final LocalNotification _localNotification = LocalNotification();

  final _formKey = GlobalKey<FormState>();
  String _email;
  String _password;

  String _errMessage;
  bool _isLoading;

  bool _isIos;

  bool _validateAndSave() {
    final FormState _formState = _formKey.currentState;
    if (_formState.validate()) {
      _formState.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    setState(() {
      _errMessage = '';
      _isLoading = true;
    });
    if (_validateAndSave()) {
      FirebaseUser _user;
      try {
        _user = await widget.auth.signIn(_email, _password);
        _localNotification.showSilentNotification(title: 'Welcome back', body: '${_user.displayName}');
        setState(() {
          _isLoading = false;
        });
        _navigationService.navigateTo(routes.HomeRoute);
      } catch (error) {
        setState(() {
          _isLoading = false;
          _errMessage = _isIos ? error.detail : error.message;
        });
        displayDialog(context, 'Sign in', _errMessage);
      }
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _isIos = Theme.of(context).platform == TargetPlatform.iOS;
    final Widget _pageTitle = Container(
      padding: EdgeInsets.only(right: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Welcome...',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 45.0,
            ),
          ),
          Text(
            'We missed you',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18.0,
              fontWeight: FontWeight.w500,
            ),
          )
        ],
      ),
    );
    final Widget _forgotPassword = FlatButton(
      onPressed: () {
        _toastLocal.showTop('msg');
        // showDialog(
        //     context: context,
        //     builder: (context) {
        //       return AlertDialog(
        //         content: TextField(
        //           controller: _textFieldController,
        //           decoration: InputDecoration(hintText: Constants.enterEmail),
        //         ),
        //         actions: <Widget>[
        //           new FlatButton(
        //             child: new Text(Constants.cancel.toUpperCase()),
        //             onPressed: () {
        //               Navigator.of(context).pop();
        //             },
        //           ),
        //           new FlatButton(
        //             child: new Text(Constants.confirm.toUpperCase()),
        //             onPressed: () {
        //               widget.auth
        //                   .resetPassword(_textFieldController.text)
        //                   .then((_) {
        //                 Navigator.of(context).pop();
        //                 displayDialog(context, Constants.success,
        //                     Constants.resetedPassword);
        //               });
        //             },
        //           )
        //         ],
        //       );
        //     });
      },
      child: Center(
        child: Text(
          Constants.forgotPassword,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
        ),
      ),
    );
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 150, left: 30),
          decoration: BoxDecoration(gradient: primaryGradient),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _pageTitle,
              Container(
                padding: EdgeInsets.only(right: 30),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'email',
                          labelStyle: TextStyle(color: Colors.white),
                          prefixIcon: Icon(
                            LineIcons.envelope,
                            color: Colors.white,
                          ),
                          focusedBorder: StyleConfig.underLineInputStyle,
                        ),
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(color: Colors.white),
                        validator: (value) =>
                            value.isEmpty ? 'please type your email' : null,
                        onSaved: (value) => _email = value.trim(),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'password',
                          labelStyle: TextStyle(color: Colors.white),
                          prefixIcon: Icon(
                            LineIcons.key,
                            color: Colors.white,
                          ),
                          focusedBorder: StyleConfig.underLineInputStyle,
                        ),
                        keyboardType: TextInputType.text,
                        style: TextStyle(color: Colors.white),
                        obscureText: true,
                        validator: (value) =>
                            value.isEmpty ? 'please type your password' : null,
                        onSaved: (value) => _password = value.trim(),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 30),
              Container(
                padding: EdgeInsets.only(right: 30),
                child: new PrimaryBtn(
                  onPressCb: _validateAndSubmit,
                  titleBtn: 'sign in',
                ),
              ),
              SizedBox(height: 30),
              Align(
                alignment: Alignment.centerRight,
                child: RaisedButton(
                  padding: EdgeInsets.fromLTRB(40, 16, 30, 16),
                  color: Colors.white,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      bottomLeft: Radius.circular(30),
                    ),
                  ),
                  onPressed: widget.callback,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        'Sign up'.toUpperCase(),
                      ),
                      SizedBox(width: 10),
                      Icon(LineIcons.arrow_right, size: 18)
                    ],
                  ),
                ),
              ),
              SizedBox(height: 40),
              _forgotPassword,
            ],
          ),
        ),
      ),
    );
  }
}
