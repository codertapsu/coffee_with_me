class RequestResponse{
  final dynamic results;
  final String error;

  RequestResponse(this.results, this.error);

  RequestResponse.fromJson(dynamic json): results = json['results'], error = '';

  RequestResponse.withError(String errorValue) : results = null, error = errorValue;
}
