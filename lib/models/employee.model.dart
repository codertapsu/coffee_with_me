import 'package:equatable/equatable.dart';

class Employee  extends Equatable{
  final int id;
  final String login;
  final String type;
  final String avatarUrl;
  Employee({this.id, this.login, this.type, this.avatarUrl}):super([id, login, type, avatarUrl]);
  factory Employee.fromJson(Map<String, dynamic> json) => _itemFromJson(json);
}

Employee _itemFromJson(Map<String, dynamic> json) {
  return Employee(
      id: json['id'] as int,
      login: json['login'] as String,
      type: json['type'] as String,
      avatarUrl: json['avatar_url'] as String);
}
