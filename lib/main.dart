import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:coffee_with_me/utils/colors.dart';
import 'package:coffee_with_me/services/locatior.service.dart';
import 'package:coffee_with_me/theme.dart';
import 'package:coffee_with_me/services/life_cycle_manager.dart';
import 'package:coffee_with_me/services/navigation.service.dart';
import 'package:coffee_with_me/routing/router.dart' as router;
import 'package:coffee_with_me/routing/routes.dart' as routesPath;
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(statusBarColor: primaryDark),
  );
  await DotEnv().load('.env');
  setUpLocator();
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LifeCycleManager(
      child: new MaterialApp(
        title: 'Coffee with me',
        debugShowCheckedModeBanner: false,
        theme: buildThemeData(),
        navigatorKey: locator<NavigationService>().navigatorKey,
        onGenerateRoute: router.generateRoute,
        initialRoute: routesPath.LandingRoute,
      ),
    );
  }
}
