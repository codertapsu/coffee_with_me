import 'package:coffee_with_me/bloc/bloc.dart';
import 'package:coffee_with_me/helper/validator.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc with Validator implements BlocBase {
  final _email = BehaviorSubject<String>();
  Stream<String> get email => _email.stream.transform(validateEmail);
  Function(String) get emailChanges => _email.sink.add;

  final _password = BehaviorSubject<String>();
  Stream<String> get password => _password.stream;
  Function(String) get passwordChanges => _password.sink.add;

  final _firstName = BehaviorSubject<String>();
  Stream<String> get firstName => _firstName.stream;
  Function(String) get firstNameChanges => _firstName.sink.add;

  final _lastName = BehaviorSubject<String>();
  Stream<String> get lastName => _lastName.stream;
  Function(String) get lastNameChanges => _lastName.sink.add;

  Stream<bool> get submitSignUpButtonValid => Observable.combineLatest4(
      firstName,
      lastName,
      email,
      password,
      (firstNameValue, lastNameValue, emailValue, passwordValue) => true);
  Map<String, String> submitSignUp() {
    final validEmail = _email.value;
    final validPassword = _password.value;
    final validFirstName = _firstName.value;
    final validLastName = _lastName.value;
    return {
      'email': validEmail,
      'password': validPassword,
      'firstName': validFirstName,
      'lastName': validLastName
    };
  }

  @override
  dispose() {
    _email.close();
    _password.close();
    _firstName.close();
    _lastName.close();
  }
}
