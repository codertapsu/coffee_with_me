import 'package:coffee_with_me/screens/sign_screen.dart';
import 'package:flutter/material.dart';
import 'package:coffee_with_me/routing/routes.dart' as routes;
import 'package:coffee_with_me/screens/landing.dart';
import 'package:coffee_with_me/screens/home.dart';

Route<dynamic> generateRoute(RouteSettings setting) {
  switch (setting.name) {
    case routes.LandingRoute:
      return MaterialPageRoute(builder: (ctx) => LandingPage());
    case routes.SignRoute:
      return MaterialPageRoute(builder: (ctx) => SignPage());
    // case signUpRoute:
    //   return MaterialPageRoute(builder: (ctx) => SignUpPage());
    // case resetPasswordRoute:
    //   return MaterialPageRoute(builder: (ctx) => ResetPasswordPage());
    case routes.HomeRoute:
      // var arrgumentPassByNavigationService = setting.arguments;
      // return MaterialPageRoute(builder: (ctx) => HomePage(data: arrgumentPassByNavigationService));
      return MaterialPageRoute(builder: (ctx) => HomePage());
    // case userDetailRoute:
    //   return MaterialPageRoute(builder: (ctx) => UserDetailPage());
    default:
      return MaterialPageRoute(builder: (ctx) => LandingPage());
  }
}
